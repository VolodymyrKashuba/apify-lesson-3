# Apify lesson lll



### Quiz answers

<!-- toc start -->
- How do you allocate more CPU for your actor run?
  - By increasing memory usage
- Which are the default storages an actor run is allocated (connected to)?
  - Datasets , key_value_stores and request_queues

- Can you change the memory allocated to a running actor?
  - Yes
- How can you run an actor with Puppeteer in a headful (non-headless) mode?
  - By setting useChrome : true and headless: false options in launch context
- Can you change the memory allocated to a running actor?
  - Yes in actors source tab in options in options tab
- Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)
  -  4GB ram and 1 CPU core
- What is the difference between RUN and CMD Dockerfile commands?
  - RUN lets you execute commands inside of your Docker image.
  - CMD lets you define a default command to run when your container starts.
- Does your Dockerfile need to contain a CMD command (assuming we don't want to use ENTRYPOINT which is similar)? If yes or no, why?
   - Yes cause trying to run an image which doesn't have an ENTRYPOINT or CMD declared will result in an error
- How does the FROM command work and which base images Apify provides?
  - FROM instruction specifies the underlying OS architecture that you are gonna use to build the image.
  - Apify Base Docker images
    - apify/actor-node
    - apify/actor-node-puppeteer-chrome
    - apify/actor-node-playwright-chrome
 <!-- toc end -->
